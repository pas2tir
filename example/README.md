# HOWTO

## Generate Makefile
```
pas2tir.sh [targets...] < [JSON config file] > Makefile
```

### Example
```
./pas2tir.sh devel graphical < base.json > Makefile
```

## Compare current and desired configuration
```
make status
```

## Apply changes
```
make install-pkgs files templates
```
